## Summary so far { data-transition="fade none" }

* `Choice` is a subclass of `Profunctor`
* `Choice` has `left` and `right` operations
* `Identity` is `Applicative`
* `Const a` is `Applicative`

