## Profunctor { data-transition="fade none" }

###### Profunctor has two interesting subclasses

* `Strong` and `Choice` are subclasses of `Profunctor`
* For the purposes of the view/update problem, we only need detail on one
* Specifically, `Choice`
* The other, `Strong`, is provided only for completeness

---

## Profunctor

###### `Strong`

* `Strong` provides two operations, `first` and `second`
* However, one can always be written using the other
* Pay particular attention to the products
* e.g. `(a, b)` means *"`a` and `b`"*
* Algebraically, `a * b`

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Profunctor p => Strong p where
  first  :: p a b -> p (a, c) (b, c)
  second :: p a b -> p (c, a) (c, b)
</code></pre>

---

## Profunctor

###### `Strong` exercises

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Profunctor p => Strong p where
  first  :: p a b -> p (a, c) (b, c)
  second :: p a b -> p (c, a) (c, b)

-- 1. Write `first` using `second`
-- 2. Write `second` using `first`
-- 3.
instance Strong (->) where
-- 4.
instance Monad k => Strong (ReaderT k) where
</code></pre>

<hr>

<div style="font-size:large">
  Don't forget that you also have `dimap`
</div>

---

## Profunctor

###### The `Either` data type

* `Either a b` is a data type that represents *"either `a` or `b`"*
* Algebraically, `a + b`

<pre style="background-color: black; color: white">
> :info Either
data Either a b = Left a | Right b
</pre>

---

## Profunctor

###### `Choice`

* `Choice` provides two operations, `left` and `right`
* However, one can always be written using the other
* Pay particular attention to the sums
* e.g. `Either a b` means *"`a` or `b`"*
* Algebraically, `a + b`

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Profunctor p => Choice p where
  left  :: p a b -> p (Either a c) (Either b c)
  right :: p a b -> p (Either c a) (Either c b)
</code></pre>

---

## Profunctor

###### `Strong` and `Choice`

* The difference between `Strong` and `Choice`
* Algebraically, the difference between multiplication and addition
* For our purposes, we will remain focused on `Choice`

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Profunctor p => Strong p where
  first  :: p a b -> p (a, c) (b, c)
  second :: p a b -> p (c, a) (c, b)

class Profunctor p => Choice p where
  left  :: p a b -> p (Either a c) (Either b c)
  right :: p a b -> p (Either c a) (Either c b)
</code></pre>

---

## Profunctor

###### `Choice` exercises

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Profunctor p => Choice p where
  left  :: p a b -> p (Either a c) (Either b c)
  right :: p a b -> p (Either c a) (Either c b)

-- 1. Write `left` using `right`
-- 2. Write `right` using `left`
-- 3.
instance Choice (->) where
-- 4.
instance Monad k => Choice (ReaderT k) where
</code></pre>

<hr>

<div style="font-size:large">
  Don't forget that you also have `dimap`
</div>

---

## Profunctor

###### `Choice`

* Put `Choice` away in your pocket
* We will see it again soon

