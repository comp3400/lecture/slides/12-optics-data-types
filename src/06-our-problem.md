## Immutable Data Types { data-transition="fade none" }

###### Back to our problem 

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Vehicle =
  Vehicle
    String -- make
    VehicleRegistration

data VehicleRegistration =
  VehicleRegistration
    String -- reg number
    Person -- owner

data Person =
  Person
    String -- name
    Int    -- age
</code></pre>

---

## Immutable Data Types

###### We write a `birthday` function

<div style="font-size:large">
  This is *very clumsy* code
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
birthday v = case v of
  Vehicle mk reg ->
    case reg of
      VehicleRegistration num own ->
        case own of
          Person nm age ->
            Vehicle mk (
              VehicleRegistration num (
                Person nm (
                  age+1
                )
              )
            )
</code></pre>

---

## Immutable Data Types

* We will start toward a solution by writing a `modify` operation
* This will be written at one level deep
* *"modify the `Int` age in the `Person`"*

<pre><code class="language-haskell hljs" data-trim data-noescape>
modifyAge ::
  (Int -> Int)
  -> Person
  -> Person
modifyAge k (Person nm age) =
  Person nm (k age)
</code></pre>

---

## Immutable Data Types

* We can write another `modify` operation 
* *"modify the `Person` owner in the `VehicleRegistration`"*

<pre><code class="language-haskell hljs" data-trim data-noescape>
modifyOwner ::
  (Person -> Person)
  -> VehicleRegistration
  -> VehicleRegistration
modifyOwner k (VehicleRegistration num own) =
  VehicleRegistration num (k own)
</code></pre>

---

## Immutable Data Types

* and another 
* *"modify the `VehicleRegistration` in the `Vehicle`"*

<pre><code class="language-haskell hljs" data-trim data-noescape>
modifyRegistration ::
  (VehicleRegistration -> VehicleRegistration)
  -> Vehicle
  -> Vehicle
modifyRegistration k (Vehicle mk reg) =
  Vehicle mk (k reg)
</code></pre>

---

## Immutable Data Types

<div style="font-size:large">
  These all have similar structure
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
modifyAge ::
  (Int -> Int)
  -> Person
  -> Person
modifyOwner ::
  (Person -> Person)
  -> VehicleRegistration
  -> VehicleRegistration
modifyRegistration ::
  (VehicleRegistration -> VehicleRegistration)
  -> Vehicle
  -> Vehicle
</code></pre>

---

## Immutable Data Types

<div style="font-size:large">
  These all have similar structure &#8594; type alias
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
type Modify a b = (b -> b) -> a -> a
modifyAge :: Modify Person Int
modifyOwner :: Modify VehicleRegistration Person
modifyRegistration :: Modify Vehicle VehicleRegistration
</code></pre>

---

## Immutable Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
type Modify a b = (b -> b) -> a -> a
modifyAge :: Modify Person Int
modifyOwner :: Modify VehicleRegistration Person
modifyRegistration :: Modify Vehicle VehicleRegistration
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- what is its type?
modifyRegistration . modifyOwner . modifyAge
-- reminder:
--   (.) :: (b -> c) -> (a -> b) -> a -> c
</code></pre>

---

## Immutable Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
type Modify a b = (b -> b) -> a -> a
modifyAge :: Modify Person Int
modifyOwner :: Modify VehicleRegistration Person
modifyRegistration :: Modify Vehicle VehicleRegistration
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
modifyVehicleOwnerAge :: Modify Vehicle Int
modifyVehicleOwnerAge =
  modifyRegistration . modifyOwner . modifyAge
</code></pre>

---

## Immutable Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
modifyVehicleOwnerAge :: Modify Vehicle Age
modifyVehicleOwnerAge =
  modifyRegistration . modifyOwner . modifyAge
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
birthday =
  modifyVehicleOwnerAge (+1)
</code></pre>

---

## Immutable Data Types

* We have a solution for this specific problem
  * The `modify` operation
  * The nesting relationship is *"one A has exactly one B and some other things"*
* What if we want more than a `modify` operation?
* How about a getter?
* *"get the one B out of the one A"*

---

## Immutable Data Types

###### `modify`

* Recall that `Identity` is neutral &mdash; it "does nothing"
* Inserting `Identity` into a type signature has no effect

<pre><code class="language-haskell hljs" data-trim data-noescape>
modify :: (b -> b) -> a -> a
</code></pre>

---

## Immutable Data Types

###### `modify`

* Recall that `Identity` is neutral &mdash; it "does nothing"
* Inserting `Identity` into a type signature has no effect

<pre><code class="language-haskell hljs" data-trim data-noescape>
modify :: (b -> Identity b) -> a -> Identity a
</code></pre>

---

## Immutable Data Types

###### `modify`

* With our new `modify` operation, we can recover the previous one
* We have generalised `modify`
* Let's write `modifyAge` again

<pre><code class="language-haskell hljs" data-trim data-noescape>
modifyAge ::
  (Int -> Identity Int)
  -> Person
  -> Identity Person
modifyAge k (Person nm age) =
  fmap (\age' -> Person nm age') (k age)
</code></pre>

---

## Immutable Data Types

###### `modify`

* We have only used `fmap` on `Identity`
* That is, nothing specific to `Identity`
* We can generalise `modify` further to any `Functor`

<pre><code class="language-haskell hljs" data-trim data-noescape>
modifyAge ::
  (Int -> Identity Int)
  -> Person
  -> Identity Person
modifyAge k (Person nm age) =
  <mark>fmap</mark> (\age' -> Person nm age') (k age)
</code></pre>

---

## Immutable Data Types

###### `modify`

<pre><code class="language-haskell hljs" data-trim data-noescape>
modify :: Functor f => (b -> f b) -> a -> f a
</code></pre>

---

## Immutable Data Types

###### `modify`

* OK, given modify, can we write a *`get`* function?
* *"one A has exactly one B and some other things"*
* But can we can the B out of the A?

<pre><code class="language-haskell hljs" data-trim data-noescape>
modify :: Functor f => (b -> f b) -> a -> f a

get :: a -> b
get = ?
</code></pre>

---

## Immutable Data Types

###### `modify`

<div style="font-size:x-large">
  Recall `Const`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Const x y = Const x
getConst :: Const x y -> x
getConst (Const x) = x
instance Functor (Const x) where

modify :: Functor f => (b -> f b) -> a -> f a

get :: a -> b
get = ?
</code></pre>

---

## Immutable Data Types

###### `modify`

<div style="font-size:x-large">
  This can be *any* functor
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Const x y = Const x
getConst :: Const x y -> x
getConst (Const x) = x
instance Functor (Const x) where

modify :: Functor <mark>f</mark> => (b -> <mark>f</mark> b) -> a -> <mark>f</mark> a

get :: a -> b
get = ?
</code></pre>

---

## Immutable Data Types

###### `modify`

<div style="font-size:x-large">
  Such as this one
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Const x y = Const x
getConst :: Const x y -> x
getConst (Const x) = x
instance Functor (<mark>Const x</mark>) where

modify :: Functor <mark>f</mark> => (b -> <mark>f</mark> b) -> a -> <mark>f</mark> a

get :: a -> b
get = ?
</code></pre>

---

## Immutable Data Types

###### `modify`

<div style="font-size:x-large">
  Such as this one
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Const x y = Const x
getConst :: Const x y -> x
getConst (Const x) = x
instance Functor (Const x) where

modify :: (b -> Const x b) -> a -> Const x a

get :: a -> b
get = ?
</code></pre>

---

## Immutable Data Types

###### `modify`

<div style="font-size:x-large">
  Let's write `get`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Const x y = Const x
getConst :: Const x y -> x
getConst (Const x) = x
instance Functor (Const x) where

modify :: (b -> Const x b) -> a -> Const x a

get :: a -> b
get = <mark>_</mark>
</code></pre>

---

## Immutable Data Types

###### `modify`

<div style="font-size:x-large">
  Let's write `get`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Const x y = Const x
getConst :: Const x y -> x
getConst (Const x) = x
instance Functor (Const x) where

modify :: (b -> Const x b) -> a -> Const x a

get :: a -> b
get = \a -> _hole1 (modify _hole2 a)
</code></pre>

<pre style="background-color: black; color: white">
-- note: x can be anything
_hole1 :: a -> Const x a
_hole1 :: a -> Const a a
_hole2 :: Const x a -> b
_hole2 :: Const b a -> b
</pre>

---

## Immutable Data Types

###### `modify`

<div style="font-size:x-large">
  Let's write `get`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Const x y = <mark>Const</mark> x
<mark>getConst</mark> :: Const x y -> x
getConst (Const x) = x
instance Functor (Const x) where

modify :: (b -> Const x b) -> a -> Const x a

get :: a -> b
get = \a -> _hole1 (modify _hole2 a)
</code></pre>

<pre style="background-color: black; color: white">

_hole1 :: Const b a -> b
_hole1 = <mark>Const</mark>
_hole2 :: a -> Const a a
_hole2 = <mark>getConst</mark>

</pre>

---

## Immutable Data Types

###### `modify`

<div style="font-size:x-large">
  Using `modify` we can write `get`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Const x y = Const x
getConst :: Const x y -> x
getConst (Const x) = x
instance Functor (Const x) where

modify :: (b -> Const x b) -> a -> Const x a

get :: a -> b
get = \a -> getConst (modify Const a)
</code></pre>

---

## Immutable Data Types

###### `modify`

<div style="font-size:x-large">
  Here is `modify` again
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
modify :: Functor f => (b -> f b) -> a -> f a
</code></pre>

<hr>

* This particular structure is well-known
* It is called a **Lens**
* We will define it using a type-alias

---

## Immutable Data Types

###### Lens

<div style="font-size:x-large">
  Was `modify`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
modify :: Functor f => (b -> f b) -> a -> f a
</code></pre>

<div style="font-size:x-large">
  Now `Lens`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
{-# LANGUAGE RankNTypes #-}
-- ^ you will need this as the top of your source file ^

type Lens a b = forall f. Functor f => (b -> f b) -> a -> f a
</code></pre>

---

## Immutable Data Types

###### Lens

<pre><code class="language-haskell hljs" data-trim data-noescape>
type Lens a b = forall f. Functor f => (b -> f b) -> a -> f a
</code></pre>

* we can take `Lens A B` to denote *"one A has exactly one B and some other things"*
* using `Lens A B` we can write `get :: A -> B`  using `(Const x)` as the functor
* using `Lens A B` we can get back to `modify` using `Identity` as the functor

---

## Immutable Data Types

###### Lens exercises

<pre><code class="language-haskell hljs" data-trim data-noescape>
{-# LANGUAGE RankNTypes #-}
type Lens a b =
  forall f. Functor f => (b -> f b) -> a -> f a

registration :: Lens Vehicle VehicleRegistration
registration = _

owner :: Lens VehicleRegistration Person
owner = _

age :: Lens Person Int
age = _

-- use the previous three lenses
vehicleOwnerAge :: Lens Vehicle Int
vehicleOwnerAge = _
</code></pre>

---

## Immutable Data Types

###### Lens

* `Lens A B` &mdash; *"one A has exactly one B and some other things"*
* We have other relationships to consider
  * one A has zero or many B and/or some other things
  * one A has exactly one B or some other things
  * one A has exactly one B and nothing else
* `Lens` will not solve these other relationships

---

## Immutable Data Types

###### Relationships

* one A has exactly one B and some other things
* one A has zero or many B and/or some other things
* one A has exactly one B or some other things
* one A has exactly one B and nothing else
* **Let's express these relationships as a hierarchy**

---

## Immutable Data Types

###### Relationships

<div style="text-align:center">
  <a href="images/lens-heirarchy1.png">
    <img src="images/lens-heirarchy1.png" alt="Lens/relationship hierarchy" style="width:640px"/>
  </a>
</div>

---

## Immutable Data Types

###### Relationships

<div style="text-align:center">
  <a href="images/lens-heirarchy2.png">
    <img src="images/lens-heirarchy2.png" alt="Lens/relationship hierarchy with label" style="width:640px"/>
  </a>
</div>

---

## Immutable Data Types

###### Another look at `Lens`

<pre><code class="language-haskell hljs" data-trim data-noescape>
type Lens a b =
  forall f. Functor f => (b -> f b) -> a -> f a
</code></pre>

<hr>

<div style="font-size:x-large">
  In a more general form
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
type Lens a b =
  p b (f b) -> p a (f a)
</code></pre>

* Where:
  * `p ~ (->)`
  * `Functor f =>`

---

## Immutable Data Types

###### Another look at `Lens`

<pre><code class="language-haskell hljs" data-trim data-noescape>
type Lens a b =
  p b (f b) -> p a (f a)
</code></pre>

<hr>

<div style="font-size:x-large">
**THIS** is the part that gives us the relationship, *"has exactly one and some other things"*
</div>

<pre style="background-color: black; color: white">
    <mark>p ~ (->)</mark>
    <mark>Functor f =></mark>
</pre>

---

## Immutable Data Types

###### `Lens` is a specific type of `Optic`

<pre><code class="language-haskell hljs" data-trim data-noescape>
type Optic p f a b =
  p b (f b) -> p a (f a)

type Lens a b =
  forall f. Functor f => Optic (->) f a b
</code></pre>

<hr>

<div style="font-size:x-large">
**THIS** is the part that gives us the relationship, *"has exactly one and some other things"*
</div>

<pre style="background-color: black; color: white">
    <mark>p ~ (->)</mark>
    <mark>Functor f =></mark>
</pre>

---

## Immutable Data Types

###### `Lens` is an `Optic`

<div style="text-align:center">
  <a href="images/lens-heirarchy3.png">
    <img src="images/lens-heirarchy3.png" alt="Lens/relationship hierarchy" style="width:640px"/>
  </a>
</div>

---

## Immutable Data Types

###### `Lens` is to product types

* *"has exactly one **AND** some other things"*
* Let's write `modify` for *"has exactly one **OR** some other things"*
* We will specialise the type first, then work toward generalisation

---

## Immutable Data Types

###### OR

<div style="font-size:x-large">
  Suppose this data type. It is one of:
</div>

* `Int` or
* `String` or
* `Char`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt Int
  | IsString String
  | IsChar Char
</code></pre>

---

## Immutable Data Types

###### OR

<div style="font-size:x-large">
  Modify the `Int`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
modify ::
  (Int -> Int) -> IntOrStringOrChar -> IntOrStringOrChar
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
-- the same trick with Identity
modify ::
  (Int -> Identity Int) -> IntOrStringOrChar -> Identity IntOrStringOrChar
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
toEither_Int ::
  IntOrStringOrChar -> Either Int (Either String Char)
toEither_Int (IsInt i) = Left i
toEither_Int (IsString s) = Right (Left s)
toEither_Int (IsChar c) = Right (Right c)

fromEither_Int ::
  Either (Identity Int) (Either String Char) -> Identity IntOrStringOrChar
fromEither_Int (Left i) = IsInt &lt;$> i
fromEither_Int (Right (Left s)) = pure (IsString s)
fromEither_Int (Right (Right c)) = pure (IsChar c)
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
toEither_Int ::
  IntOrStringOrChar -> Either Int (Either String Char)
toEither_Int (IsInt i) = Left i
toEither_Int (IsString s) = Right (Left s)
toEither_Int (IsChar c) = Right (Right c)

fromEither_Int ::
  Either (<mark>Identity</mark> Int) (Either String Char) -> <mark>Identity</mark> IntOrStringOrChar
fromEither_Int (Left i) = IsInt <mark>&lt;$></mark> i
fromEither_Int (Right (Left s)) = <mark>pure</mark> (IsString s)
fromEither_Int (Right (Right c)) = <mark>pure</mark> (IsChar c)
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
toEither_Int ::
  IntOrStringOrChar -> Either Int (Either String Char)
toEither_Int (IsInt i) = Left i
toEither_Int (IsString s) = Right (Left s)
toEither_Int (IsChar c) = Right (Right c)

fromEither_Int ::
  Applicative f =>
  Either (<mark>f</mark> Int) (Either String Char) -> <mark>f</mark> IntOrStringOrChar
fromEither_Int (Left i) = IsInt <mark>&lt;$></mark> i
fromEither_Int (Right (Left s)) = <mark>pure</mark> (IsString s)
fromEither_Int (Right (Right c)) = <mark>pure</mark> (IsChar c)
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
toEither_Int ::
  IntOrStringOrChar -> Either Int (Either String Char)

fromEither_Int ::
  Applicative f =>
  Either (f Int) (Either String Char) -> f IntOrStringOrChar
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
modify ::
  (Int -> Identity Int) -> IntOrStringOrChar -> Identity IntOrStringOrChar
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
toEither_Int ::
  IntOrStringOrChar -> Either Int (Either String Char)

fromEither_Int ::
  Applicative f =>
  Either (f Int) (Either String Char) -> f IntOrStringOrChar
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
modify ::
  (Int -> <mark>Identity</mark> Int) -> IntOrStringOrChar -> <mark>Identity</mark> IntOrStringOrChar
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
toEither_Int ::
  IntOrStringOrChar -> Either Int (Either String Char)

fromEither_Int ::
  Applicative f =>
  Either (f Int) (Either String Char) -> f IntOrStringOrChar
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
modify ::
  Applicative f =>
  (Int -> f Int) -> IntOrStringOrChar -> f IntOrStringOrChar
modify k =
  dimap toEither_Int fromEither_Int (left k)
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
toEither_Int ::
  IntOrStringOrChar -> Either Int (Either String Char)

fromEither_Int ::
  Applicative f =>
  Either (f Int) (Either String Char) -> f IntOrStringOrChar
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
modify ::
  Applicative f =>
  (Int <mark>-></mark> f Int) -> IntOrStringOrChar <mark>-></mark> f IntOrStringOrChar
modify k =
  <mark>dimap</mark> toEither_Int fromEither_Int (<mark>left</mark> k)
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
toEither_Int ::
  IntOrStringOrChar -> Either Int (Either String Char)

fromEither_Int ::
  Applicative f =>
  Either (f Int) (Either String Char) -> f IntOrStringOrChar
</code></pre>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
modify ::
  (<mark>Choice p</mark>, Applicative f) =>
  <mark>p</mark> Int (f Int) -> <mark>p</mark> IntOrStringOrChar (f IntOrStringOrChar)
modify k =
  <mark>dimap</mark> toEither_Int fromEither_Int (<mark>left</mark> k)
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<hr>

* We have written `modify` for the relationship, *"one has exactly one **or** some other things"*
* It is yet another `Optic` with constraints
* `Choice p =>`
* `Applicative f =>`

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
modify ::
  (Choice p, Applicative f) =>
  p Int (f Int) -> p IntOrStringOrChar (f IntOrStringOrChar)
modify k =
  dimap toEither_Int fromEither_Int (left k)
</code></pre>

---

## Immutable Data Types

###### OR

* This is called a `Prism`
* Prisms work on **sum types** in a similar way that Lenses work on **product types**

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
type Optic p f a b =
  p b (f b) -> p a (f a)

type Lens a b =
  forall f. Functor f => Optic (->) f a b

type Prism a b =
  forall p f. (Choice p, Applicative f) => Optic p f a b
</code></pre>

---

## Immutable Data Types

###### OR

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntOrStringOrChar =
  IsInt <mark>Int</mark>
  | IsString String
  | IsChar Char
</code></pre>

<hr>

<pre style="font-size:x-large"><code class="language-haskell hljs" data-trim data-noescape>
modify :: Prism IntOrStringOrChar Int
</code></pre>

<hr>

<div style="font-size:x-large">
  *"`IntOrStringOrChar` has exactly one `Int` or some other things"*
</div>

---

## Immutable Data Types

###### Relationships

<div style="text-align:center">
  <a href="images/lens-heirarchy4.png">
    <img src="images/lens-heirarchy4.png" alt="Lens/relationship hierarchy" style="width:640px"/>
  </a>
</div>

---

## Immutable Data Types

###### Relationships

* `Lens A B` &mdash; *"one A has exactly one B and some other things"*
  * `p ~ (->)`
  * `Functor f =>`
* `Prism A B` &mdash; *"one A has exactly one B or some other things"*
  * `Choice p =>`
  * `Applicative p =>`
* one A has zero or many B and/or some other things
* one A has exactly one B and nothing else

---

## Immutable Data Types

###### Relationships

* Rather than labour over the other two deriving `modify`
* Here they are
* They are all optics
* However, note in particular the different constraints

---

## Immutable Data Types

###### Relationships

* `Lens A B` &mdash; *"one A has exactly one B and some other things"*
  * `p ~ (->)`
  * `Functor f =>`
* `Prism A B` &mdash; *"one A has exactly one B or some other things"*
  * `Choice p =>`
  * `Applicative p =>`
* `Traversal A B` &mdash; *"one A has zero or many B and/or some other things"*
  * `p ~ (->)`
  * `Applicative f =>`
* `Isomorphism A B` &mdash; *"one A has exactly one B and nothing else"*
  * `Profunctor p =>`
  * `Functor f =>`

---

## Immutable Data Types

###### Relationships

<div style="text-align:center">
  <a href="images/lens-heirarchy5.png">
    <img src="images/lens-heirarchy5.png" alt="Lens/relationship hierarchy" style="width:640px"/>
  </a>
</div>

---

## Optics

###### Exercises

<div style="font-size:x-large">
  <a href="https://gitlab.com/comp3400/lecture/code/12-optics-data-types">https://gitlab.com/comp3400/lecture/code/12-optics-data-types</a>
</div>

