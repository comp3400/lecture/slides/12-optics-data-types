## Revisiting a couple of data types { data-transition="fade none" }

###### This is just a reminder

---

## Identity

###### `Identity` is `Applicative`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Identity a = Identity a

instance Functor Identity where &hellip;
instance Applicative Identity where &hellip;
</code></pre>

<hr>

<div style="font-size:large">
  *`Identity` is also a `Monad` but that's irrelevant today*
</div>

---

## Const

###### `Const` is `Applicative`

* We have seen `Const` in prac sessions
* Given `Monoid a`, then `Const a` is `Applicative`
* If this is a little unfamiliar, set these instances as exercises

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Const a b = Const a

instance Functor (Const a) where &hellip;
instance Monoid a => Applicative (Const a) where &hellip;
</code></pre>

<hr>

<div style="font-size:large">
  *`Const a` is not a `Monad`*
</div>

