## Revision { data-transition="fade none" }

###### Traversable

* The `Traversable` type class generalises *"iteration"*
* `Traversable` is a subclass of both `Foldable` and `Functor`
* `Traversable` has many instances e.g.
* You may see it defined with `sequence` &mdash; a function that is equal in power to `traverse`

<pre><code class="language-haskell hljs" data-trim data-noescape>
class (Functor t, Foldable t) => Traversable t where
  traverse ::
    (Traversable t, Applicative k) =>
    (a -> k b) -> t a -> k (t b

instance Traversable [] where
instance Traversable Maybe where
instance Traversable ((,) a) where
</code></pre>

---

## Revision

###### Traversable

* The `traverse` operation is ubiquitous
* `traverse` covers many general programming problems
* `Traversable` is closed under composition

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Composition f g a = Composition (f (g a))

-- this is possible
instance (Traversable f, Traversable g) =>
  Traversable (Composition f g) where
</code></pre>

---

## Revision

###### Right folds

* Right folds replace constructors in a list
* This is not done in any particular execution order

<pre><code class="language-haskell hljs" data-trim data-noescape>
foldr :: (a -> b -> b) -> b -> List a -> b
</code></pre>

---

## Revision

###### Left folds

* Left folds loop through a list
* Although there is no *actual loop*, this analogy is precise

<pre><code class="language-haskell hljs" data-trim data-noescape>
foldl :: (b -> a -> b) -> b -> List a -> b

foldl func z list =
  var r = z
  for(elem in list) {
    r = func(r, elem)
  }
  return r
</code></pre>

---

## Revision

###### Algebra

* The algebra in algebraic data types, is that same algebra that we all know and love
* Sum types correspond to addition (`|` symbol in Haskell data types)
* When we describe a data type by saying *"OR"*, we are describing addition
* Product types correspond to multiplication
* When we describe a data type by saying *"AND"*, we are describing multiplication

---

## Revision

###### Algebra

* Exponential types correspond to a function
* Specifically, the return type raised to the power of the argument type
* `Unit = 1`
* `Void = 0`
* `Optional a = 1 + a`
* <code>(Optional Bool -> Bool) = 2&#179; = 8</code>

---

## Revision

###### Algebra

* We can express regular algebraic rules with data types
* For example, equivalences:
  * the distributive law of multiplication over addition
  * the power of a power law (which gives us the curry/uncurry isomorphism)

---

## Revision

###### Nested data types

* Updating nested immutable data types is clumsy
* We have to walk down the data type, then put it all back together
* The *"view/update problem"*
* We have started exploring the parts that will lead to a solution

---

## Revision

###### Nested data types

* There are different relationships of nesting of data types
* We will focus on four in particular
  * one A has exactly one B and some other things
  * one A has zero or many B and/or some other things
  * one A has exactly one B or some other things
  * one A has exactly one B and nothing else

---

## Revision

###### Variance

* Covariant functors are those defined over a value that appears only in positive position
* Contravariant functors are those defined over a value that appears only in negative position
* Invariant functors are neither covariant nor contravariant
* Bivariant functors are both covariant and contravariant
* Invariant and bivariant functors are relatively uninteresting

---

## Revision

###### Variance and arity

* Binary functors are defined over type constructors with kind <code>* -> * -> *</code>
* For example `(->)`, `Writer`
* Binary functors may be covariant, contravariant, neither or both in each of its type variables

---

## Revision

###### Profunctor

* A profunctor is a binary functor that is contravariant in its first argument type, covariant in its next argument type
* For example
  * `(->)`
  * `Monad k => ReaderT k`

---

## Revision

###### Profunctor

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Profunctor p where
  dimap :: (a -> b) -> (c -> d) -> p b c -> p a d

instance Profunctor (->) where
data ReaderT k a b = ReaderT (a -> k b)
instance Monad k => Profunctor (ReaderT k) where
</code></pre>

